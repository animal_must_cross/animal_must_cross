﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCar : MonoBehaviour {

    [SerializeField] private Transform endPoint;
    public Vector3 startPosition;
    [SerializeField] private Pattern pattern;
    [SerializeField] private int speed = 100;
    
    public delegate void DoAction();
    DoAction doAction;

	// Use this for initialization
	void Start () {
        startPosition = transform.position;
        doAction = doActionMove;
        transform.LookAt(endPoint);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Fox"))
        {
            //Time.timeScale = 0.1f;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Fox"))  Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update () {
        doAction();
    }

    private void doActionMove ()
    {
        transform.position += speed * Time.deltaTime * transform.forward;
    }
}
