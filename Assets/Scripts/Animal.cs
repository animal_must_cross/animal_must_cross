﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Animal : MonoBehaviour {

  //  static public Animal[] animals;
    static public List<Animal> animals = new List<Animal>();
    [SerializeField] float speed = 1;
    private float inertiaRate = 0.05f;
    private float velocity = 0;
    delegate void doActionDel();
    private bool isActive = false;

    doActionDel doAction; 


    public static void toggleModes()
    {
        foreach (Animal animal in animals)
        {
            animal.toggleMode();
        }
    }
	// Use this for initialization
	void Start () {
        animals.Add(this);
        doAction = doActionVoid;
	}

    void doActionVoid()
    {

    }

    void setModeVoid()
    {
        velocity = 0;
        doAction = doActionVoid;
    }

    void doActionWalk()
    {
        transform.Translate(transform.right * velocity * Time.deltaTime);
    }

    void setModeStop()
    {
        doAction = doActionStop;
    }

    void setModeAcceleration()
    {
        doAction = doActionAcceleration;
    }

    void doActionAcceleration()
    {
        velocity += speed * inertiaRate;
        doActionWalk();
        if (velocity >= speed) setModeWalk();
    }

    void doActionStop()
    {
        velocity -= speed * inertiaRate;
        doActionWalk();
        if (velocity <= 0) setModeVoid();
    }

    public void setModeWalk()
    {
        velocity = speed;
        doAction = doActionWalk;
    }

    public void toggleMode()
    {
        if (!isActive) setModeAcceleration();
        else setModeStop();

        isActive = !isActive;
    }


	
	// Update is called once per frame
	void Update () {
        doAction();
	}
}
