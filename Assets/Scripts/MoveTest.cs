﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTest : MonoBehaviour {

    private Vector3 axeView = new Vector3(1, 0, 0);
    [SerializeField] private float speed = 60;

    public Transform effect;

	// Use this for initialization
	void Start () {
        speed += UnityEngine.Random.value*5;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            speed = 0;
            Instantiate(effect, transform.position, transform.rotation);
        }
    }

    // Update is called once per frame
    void Update () {
        transform.position += axeView * speed * Time.deltaTime;

	}
}
